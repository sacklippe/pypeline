import unittest

import pypeline


class TestGetVersion(unittest.TestCase):
    def test_get_version_regex(self):
        pattern = r"^\d+\.\d+\.\d+$"
        self.assertRegex(pypeline.get_version(), pattern)


if __name__ == "__main__":
    unittest.main()
