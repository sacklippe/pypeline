import pathlib

import toml


def get_version() -> str:
    """Get the version from pyproject.toml"""
    pyproject = pathlib.Path(__file__).parent.parent / "pyproject.toml"
    with open(pyproject) as f:
        return toml.load(f)["tool"]["poetry"]["version"]


__version__ = get_version()
