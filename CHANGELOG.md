# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.7.1](https://gitlab.com/sacklippe/pypeline/compare/v1.7.0...v1.7.1) (2023-09-27)


### Bug Fixes

* add destination copy ([2203547](https://gitlab.com/sacklippe/pypeline/commit/2203547272bc9d1feaec54342de55e3ad50bf3d7))

## [1.7.0](https://gitlab.com/sacklippe/pypeline/compare/v1.6.1...v1.7.0) (2023-09-27)


### Features

* update toml file from artifact ([b14dcae](https://gitlab.com/sacklippe/pypeline/commit/b14dcaed659464b951464a6964f3de68cba61b71))

### [1.6.1](https://gitlab.com/sacklippe/pypeline/compare/v1.6.0...v1.6.1) (2023-09-27)


### Bug Fixes

* change artifact path ([098997f](https://gitlab.com/sacklippe/pypeline/commit/098997f8a245dd87f6aecb67c2156944f07edbf5))

## [1.6.0](https://gitlab.com/sacklippe/pypeline/compare/v1.5.1...v1.6.0) (2023-09-27)

### [1.5.1](https://gitlab.com/sacklippe/pypeline/compare/v1.5.0...v1.5.1) (2023-09-27)


### Bug Fixes

* execution order ([ab9932d](https://gitlab.com/sacklippe/pypeline/commit/ab9932d452c34c1cf8f7bf44adcc62cb235d1470))
* jobs:publish:variables config should be a hash ([e7fbeaa](https://gitlab.com/sacklippe/pypeline/commit/e7fbeaa589eea7a5192920ff699e1bc690f27284))
* registry names cannot be parsed natively ([8fc136d](https://gitlab.com/sacklippe/pypeline/commit/8fc136d5530be0045b4fd0e011db81fbda5ee535))
* typo ([c9ea021](https://gitlab.com/sacklippe/pypeline/commit/c9ea021a17a11c1032df084d3e95e7528d9e3d35))

## [1.5.0](https://gitlab.com/sacklippe/pypeline/compare/v1.4.0...v1.5.0) (2023-09-27)


### Features

* execute publishing pipeline ([4cf3bc7](https://gitlab.com/sacklippe/pypeline/commit/4cf3bc7156607635d3c5ebe447d195f37795e2d3))

## [1.4.0](https://gitlab.com/sacklippe/pypeline/compare/v1.3.2...v1.4.0) (2023-09-27)


### Features

* introduce pipeline to publish to local package registry ([7e61606](https://gitlab.com/sacklippe/pypeline/commit/7e6160640d6cafa1056f8620f8490a0810cc9987))

### [1.3.2](https://gitlab.com/sacklippe/pypeline/compare/v1.3.1...v1.3.2) (2023-08-18)

### [1.3.1](https://gitlab.com/sacklippe/pypeline/compare/v1.3.0...v1.3.1) (2023-08-18)


### Bug Fixes

* remove stage from job ([4768be7](https://gitlab.com/sacklippe/pypeline/commit/4768be78b4467c367316388610889d072f62fa0b))

## [1.3.0](https://gitlab.com/sacklippe/pypeline/compare/v1.2.3...v1.3.0) (2023-08-18)


### Features

* add style check pipeline ([809419a](https://gitlab.com/sacklippe/pypeline/commit/809419a0971a6d248927a385197966f3e4c0bfa6))

### [1.2.3](https://gitlab.com/sacklippe/pypeline/compare/v1.2.2...v1.2.3) (2023-08-18)


### Bug Fixes

* wrong file extension ([3e2b4eb](https://gitlab.com/sacklippe/pypeline/commit/3e2b4ebc39995658fb47dc3c6a4ff30a17be1dd2))

### [1.2.2](https://gitlab.com/sacklippe/pypeline/compare/v1.2.1...v1.2.2) (2023-08-18)

### [1.2.1](https://gitlab.com/sacklippe/pypeline/compare/v1.2.0...v1.2.1) (2023-08-18)

## [1.2.0](https://gitlab.com/sacklippe/pypeline/compare/v1.1.0...v1.2.0) (2023-08-18)


### Features

* include coverage in testing pipeline ([29a26d3](https://gitlab.com/sacklippe/pypeline/commit/29a26d323daaa2a87ba01ba9e53de9ef285319a0))
* introduce simple test pipeline ([fd84567](https://gitlab.com/sacklippe/pypeline/commit/fd845671665965957f52a38e67e7945670adcdff))


### Bug Fixes

* regex quotes ([7a4c2c0](https://gitlab.com/sacklippe/pypeline/commit/7a4c2c06dd5e2948517d7a0168c2a67adb4d4d4d))

## [1.1.0](https://gitlab.com/sacklippe/pypeline/compare/v1.0.1...v1.1.0) (2023-08-18)


### Features

* implement minimal package logic ([62b2c96](https://gitlab.com/sacklippe/pypeline/commit/62b2c963db288082a8d1f1a142897d1269cf3d9c))

### [1.0.1](https://gitlab.com/sacklippe/pypeline/compare/v1.0.0...v1.0.1) (2023-08-17)

## 1.0.0 (2023-08-16)
