# pypeline

This package contains a mock python package, to execute and develop GitLab CI pipelines on.

## Include Pipeline

To include a pipeline into another project you can add the following into you `.gitlab-ci.yml` file:
```yaml
include: 
  - project: 'sacklippe/pypeline'
    ref: main
    file: '/.gitlab-ci/<pipeline-file>'

stages:
  - <stage-of-the-pipeline-file>
```

---

![version](https://gitlab.com/sacklippe/pypeline/-/badges/release.svg) | ![pipeline](https://gitlab.com/sacklippe/pypeline/badges/main/pipeline.svg?ignore_skipped=true) | ![coverage](https://gitlab.com/sacklippe/pypeline/badges/main/coverage.svg)
